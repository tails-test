# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2013-06-25 10:51+0300\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta date=\"Sat May 18 11:23:45 2013\"]]\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "[[!meta title=\"Tails 0.18 is out\"]]\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "[[!tag announce]]\n"
msgstr ""

#. type: Plain text
msgid "Tails, The Amnesic Incognito Live System, version 0.18, is out."
msgstr ""

#. type: Plain text
msgid "All users must upgrade as soon as possible."
msgstr ""

#. type: Plain text
#, no-wrap
msgid "[[!toc levels=1]]\n"
msgstr ""

#. type: Title #
#, no-wrap
msgid "Changes"
msgstr ""

#. type: Plain text
msgid "Notable user-visible changes include:"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"* New features\n"
"  - Support obfs3 bridges.\n"
"  - Automatically [[install a custom list of additional packages|doc/first_steps/persistence/configure#additional_packages]]\n"
"    chosen by the user at the beginning of every working session, and upgrade\n"
"    them once a network connection is established (technology preview).\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"* Iceweasel\n"
"  - Upgrade to Iceweasel 17.0.6esr-0+tails1~bpo60+1.\n"
"  - Update Torbrowser patches to current maint-2.4 branch (567682b).\n"
"  - Torbutton 1.5.2, and various prefs hacks to fix breakage.\n"
"  - HTTPS Everywhere 3.2\n"
"  - NoScript 2.6.6.1-1\n"
"  - Isolate DOM storage to first party URI, and enable DOM storage.\n"
"  - Isolate the image cache per url bar domain.\n"
"  - Update prefs to match the TBB's, fix bugs, and take advantage of\n"
"    the latest Torbrowser patches.\n"
"  - Make prefs organization closer to the TBB's, and generally clean\n"
"    them up.\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"* Bugfixes\n"
"  - Linux 3.2.41-2+deb7u2.\n"
"  - All Iceweasel prefs we set are now applied.\n"
"  - Bring back support for proxies of type other than obfsproxy.\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"* Minor improvements\n"
"  - Set `kernel.dmesg_restrict=1`, and make `/proc/<pid>/` invisible\n"
"    and restricted for other users. It makes it slightly harder for an\n"
"    attacker to gather information that may allow them to\n"
"    escalate privileges.\n"
"  - Install gnome-screenshot.\n"
"  - Add a *About Tails* launcher in the *System* menu.\n"
"  - Install GNOME accessibility themes.\n"
"  - Use *Getting started...* as the homepage for the Tails\n"
"    documentation button.\n"
"  - Disable audio preview in Nautilus.\n"
msgstr ""

#. type: Bullet: '* '
msgid "Localization: many translation updates all over the place."
msgstr ""

#. type: Plain text
msgid ""
"See the [online Changelog](https://git-tails.immerda.ch/tails/plain/debian/"
"changelog?id=0.18)  for technical details."
msgstr ""

#. type: Title #
#, no-wrap
msgid "Known issue"
msgstr ""

#. type: Plain text
msgid ""
"The web browser default search engine is Google, instead of the intended "
"localized Startpage. You may select *Startpage HTTPS* in the search engine "
"menu next to the Google icon."
msgstr ""

#. type: Title #
#, no-wrap
msgid "I want to try it / to upgrade!"
msgstr ""

#. type: Plain text
msgid "See the [[Getting started]] page."
msgstr ""

#. type: Plain text
msgid ""
"As no software is ever perfect, we maintain a list of [[problems that "
"affects the last release of Tails|support/known_issues]]."
msgstr ""

#. type: Title #
#, no-wrap
msgid "What's coming up?"
msgstr ""

#. type: Plain text
msgid "The next Tails release is scheduled for June 27."
msgstr ""

#. type: Plain text
msgid "Have a look to our [[!tails_roadmap]] to see where we are heading to."
msgstr ""

#. type: Plain text
msgid ""
"Would you want to help? As explained in our [[\"how to contribute\" "
"documentation|contribute]], there are many ways **you** can contribute to "
"Tails. If you want to help, come talk to us!"
msgstr ""
