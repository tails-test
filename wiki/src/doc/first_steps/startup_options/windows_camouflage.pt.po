# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2014-08-12 22:33+0200\n"
"PO-Revision-Date: 2014-08-26 15:49-0300\n"
"Last-Translator: Tails Developers <amnesia@boum.org>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta title=\"Windows camouflage\"]]\n"
msgstr "[[!meta title=\"Camuflagem Windows\"]]\n"

#. type: Plain text
msgid "If you are using a computer in public you may want to avoid attracting unwanted attention by changing the way Tails looks into something that resembles Microsoft Windows 8."
msgstr "Se você está usando um computador em público, você pode querer evitar atrair atenção indesejada mudando a aparência do Tails para algo que se parece com o Microsoft Windows 8."

#. type: Title =
#, no-wrap
msgid "Activate the Windows camouflage\n"
msgstr "Ativar a camuflagem Windows\n"

#. type: Plain text
#, no-wrap
msgid ""
"The Windows camouflage can be activated from [[<span class=\"application\">Tails\n"
"Greeter</span>|startup_options#tails_greeter]]:\n"
msgstr ""
"A camuflagem Windows pode ser ativada a partir do [[<span class=\"application\">Tails\n"
"Greeter</span>|startup_options#tails_greeter]]:\n"

#. type: Bullet: '1. '
msgid "When <span class=\"application\">Tails Greeter</span> appears, in the <span class=\"guilabel\">Welcome to Tails</span> window, click on the <span class=\"button\">Yes</span> button. Then click on the <span class=\"button\">Forward</span> button."
msgstr "Quando o <span class=\"application\">Tails Greeter</span> aparece, na tela que diz <span class=\"guilabel\">Bem Vindo/a ao Tails</span>, clique no botão <span class=\"button\">Sim</span>. A seguir, clique no botão <span class=\"button\">Próximo</span>."

#. type: Bullet: '2. '
msgid "In the <span class=\"guilabel\">Windows camouflage</span> section, select the <span class=\"guilabel\">Activate Microsoft Windows 8 Camouflage</span> option."
msgstr "Na seção de <span class=\"guilabel\">Camuflagem Windows</span>, selecione a opção <span class=\"guilabel\">Ativar camuflagem Microsoft Windows 8</span>."

#. type: Plain text
msgid "This is how your Tails desktop will look like:"
msgstr "Seu desktop Tails vai se paracer com algo assim:"

#. type: Plain text
#, no-wrap
msgid "[[!img windows_camouflage.jpg link=no alt=\"Tails with Windows camouflage\"]]\n"
msgstr "[[!img windows_camouflage.jpg link=no alt=\"Tails com camuflagem Windows\"]]\n"

#~ msgid ""
#~ "When Tails is starting up the Windows camouflage can be activated in\n"
#~ "[[<span class=\"application\">Tails Greeter</span>|"
#~ "startup_options#tails_greeter]]\n"
#~ "by choosing <span class=\"button\">Yes</span> to <span\n"
#~ "class=\"button\">More options?</span> and then enabling the checkbox\n"
#~ "labelled <span class=\"button\">Activate Microsoft Windows XP\n"
#~ "Camouflage</span>.\n"
#~ msgstr ""
#~ "Quando Tails estiver iniciando, a Camuflagem Windows pode ser ativada no\n"
#~ "[[<span class=\"application\">Tails Greeter</span>|"
#~ "startup_options#tails_greeter]]\n"
#~ "escolhendo <span class=\"button\">Sim</span> para<span\n"
#~ "class=\"button\">Mais opções?</span> e então habilitando o botão\n"
#~ "<span class=\"button\">Activate Microsoft Windows XP\n"
#~ "Camouflage</span>.\n"
